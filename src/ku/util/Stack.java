package ku.util;

import java.util.EmptyStackException;
/**
 * stack can fill element or remove last element in a array.
 * @author Jidapar Jettananurak
 *
 * @param <T> is array of object.
 */
public class Stack<T>{

	/**
	 * capacity is size of array.
	 * items is array that keep object.
	 */
	private int capacity;
	private T[] items;
	
	/**
	 * A constructor create array and set size of array.
	 * @param capacity is size of array.
	 */
	public Stack(int capacity){
		this.capacity = capacity;
		items = (T[])new Object[capacity];
	}
	
	/**
	 * capacity will tell size of array.
	 * @return size of array.
	 */
	public int capacity(){
		return capacity;
	}
	
	/**
	 * isEmpty check array have element or not.
	 * @return true if don't have anything in array otherwise false.
	 */
	public boolean isEmpty(){
		for(int i = 0 ; i < items.length ; i++){
			if(items[i] != null)
				return false;
		}
		return true;
	}
	
	/**
	 * isFull check array is full or not.
	 * @return true if array don't have null.
	 */
	public boolean isFull(){
		for(int i = 0 ; i < items.length ; i++){
			if(items[i] == null)
				return false;
		}
		return true;
	}
	
	/**
	 * peek give top object of array.
	 * @return object last object which have value otherwise return null.
	 */
	public T peek(){
		T item2 = null;
		if(isEmpty())
			return null;
		
		if(isFull()) {
			return items[items.length-1];
		}
		
		for(int i = 0 ; i < items.length ; i++){
			if(items[i] == null)
				item2 = items[i-1];
		}
		return item2;
	}
	
	/**
	 * pop will give value of element and remove the value in array.
	 * @return last object of array that have value.
	 */
	public T pop(){
		T keep = null;
		if(isEmpty())
			throw new EmptyStackException() ;
		
		if(isFull()) {
			keep = items[items.length-1];
			items[items.length-1] = null;
			return keep;
		}
		
		for(int i = 0 ; i < items.length ; i++){
			if(items[i] == null){
				keep = items[i-1];
				items[i-1] = null;
				break;
			}
		}
		return keep;
	}
	
	/**
	 * push fill object into array.
	 * @param obj is a element target.
	 */
	public void push(T obj){
		if(obj == null)
			throw new IllegalArgumentException(); 
		if(isFull() == false){
			for(int i = 0 ; i < items.length ; i++){
				if(items[i] == null) {
					items[i] = obj;
					break;
				}
			}
		}	
	}
	
	/**
	 * size is number of element in array that have value.
	 * @return number of element in array if not have anything in array return 0.
	 */
	public int size(){
		int num = 0;
		if(isEmpty()){
			return 0;			
		}

		for(int i = 0 ; i < items.length ; i++){
			if(items[i] != null)
				num++;
		}
		return num;
	}
}

