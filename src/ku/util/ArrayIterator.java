package ku.util;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * ArrayIterator can check array of object that have value or not.
 * @author Jidapar Jettananurak
 *
 * @param <T> is array of object.
 */
public class ArrayIterator<T> implements Iterator<T>{
	
	/**
	 * array keep array that is sent.
	 * check is a position of index in array.
	 */
	private T[ ] array;
	private int check = 0;

	/**
	 * A constructor is set array.
	 * @param array is array that want to check.
	 */
	public ArrayIterator(T[] array) {
		this.array = array;
	}

	/**
	 * hasNext check next element that have value.
	 * @return true if next element have value otherwise false. 
	 */
	public boolean hasNext() {
		for(int i = check ; i < array.length ; i++){
			if(array[i] != null)
				return true;
		}
		return false;
	}

	/**
	 * next can get element that have value.
	 * @return object of element.
	 */
	public T next() {	
		for(int i = check ; i < array.length ; i++){
			if(array[i] != null) {
				check++;
				return array[i];
			}
			check++;
		}
		throw new NoSuchElementException();
	}

	/**
	 * remove if next element have value it will be null.
	 */
	public void remove(){
		array[check-1] = null;
	}

}
